package com.hcl.bean;

public class Signin {
	private String MobileNumber;
	private String Password;
	public Signin() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Signin( String MobileNumber,  String PassWord) {
		super();
		this.MobileNumber=MobileNumber;
	
		this.Password=Password;
	}
		
	public String getMobileNumber() {
		return MobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		MobileNumber = mobileNumber;
	}

	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	@Override
	public String toString() {
		return "Signin [ MobileNumber ="+ MobileNumber+", PassWord=" + Password + "]";
	}
	

}
