package com.hcl.Dao;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.hcl.bean.BookDetails;

public class BookDetailsDao {
	private Connection con;

	private String query;
    private PreparedStatement pst;
    private ResultSet rs;
    public BookDetailsDao(Connection con) {
		
		this.con = con;
	}

    

	public List<BookDetails> getAllBooks() {
        List<BookDetails> listOfBooks = new ArrayList<>();
        try {

            query = "select * from books";
            pst =con.prepareStatement(query);
            rs = pst.executeQuery();

            while (rs.next()) {
            	BookDetails bb = new BookDetails();
                
                bb.setId(rs.getInt("id"));
                bb.setNameOfBook(rs.getString("NameOfBook"));
                bb.setStoryline(rs.getString("Storyline"));

                listOfBooks.add(bb);
            }

        } catch (SQLException e) {
            
            System.out.println(e);
        }
        return listOfBooks;
    }

	

}
