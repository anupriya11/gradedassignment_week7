package com.hcl.bean;

public class Register {
private String Name, Password, Email,MobileNumber;
	
	public Register() {
		
	}
public Register(String Name, String Password, String Email, String MobileNumber) {
	       super();
			
		this.Name = Name;
		this.Password = Password;
		this.Email = Email;
		this.MobileNumber = MobileNumber;
	}
	


	public String getName() {
		return Name;
	}

	public void setName(String name) {
		this.Name = name;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		this.Password = password;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		this.Email = email;
	}

	public String getMobileNumber() {
		return MobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.MobileNumber = mobileNumber;
	}

}
