package com.hcl.Dao;
import java.sql.*;
import com.hcl.bean.*;
import com.hcl.dbresource.*;

public class SigninDao {
	private Connection con;
	public int storeSignin(Signin signin) {
		try {
			Connection con = DbConnection.getConnection();
			PreparedStatement pstmt = con.prepareStatement("insert into login values(?,?)");
		
			pstmt.setString(2, signin.getPassword());
			pstmt.setString(3,signin.getMobileNumber());
			
			return pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("Store Method Exception "+e);
			return 0;
		}
	}

}
