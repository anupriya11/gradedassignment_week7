package com.hcl.Servlet;

import java.io.IOException;

import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hcl.bean.*;


import com.hcl.Dao.*;
import com.hcl.dbresource.DbConnection;

import com.hcl.Service.*;

/**
 * Servlet implementation class RegisterController
 */
@WebServlet("/RegisterController")
public class RegisterController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		RegisterService rs = new RegisterService();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		PrintWriter pw = response.getWriter();
		String Name = request.getParameter("username");
		String Password = request.getParameter("password");
		String Email=request.getParameter("email");
		String MobileNumber=request.getParameter("MobileNumber");
		Register reg = new Register();
		reg.setName(Name);
		reg.setPassword(Password);
		reg.setEmail(Email);
		reg.setMobileNumber(MobileNumber);
		
		RegisterService ls = new RegisterService();
		String res = ls.storeRegister(reg);
						doGet(request, response);				
		pw.println(res);
		RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
		rd.include(request, response);

		
	}


}
