package com.hcl.Dao;
import java.sql.*;

import com.hcl.bean.*;
import com.hcl.dbresource.*;

public class RegisterDao {
	public int storeRegister(Register reg) {
		try {
			Connection con = DbConnection.getConnection();
			PreparedStatement pstmt = con.prepareStatement("insert into registration values(?,?,?,?)");
			pstmt.setString(1, reg.getName());
			pstmt.setString(2, reg.getPassword());
			pstmt.setString(3, reg.getEmail());
			pstmt.setString(4, reg.getMobileNumber());
			
			return pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("Store Method Exception "+e);
			return 0;
		}
	}

}
