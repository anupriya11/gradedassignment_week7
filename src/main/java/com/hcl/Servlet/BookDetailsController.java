package com.hcl.Servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.hcl.bean.*;
import com.hcl.Service.*;
import com.hcl.Dao.*;

/**
 * Servlet implementation class BookDetails
 */
@WebServlet("/BookDetails")
public class BookDetailsController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookDetailsController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		BookDetailsService bs = new BookDetailsService();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		PrintWriter pw = response.getWriter();
		int id=Integer.parseInt(request.getParameter("id"));
		String NameOfBook= request.getParameter("NameOfBook");
		String Storyline = request.getParameter("Storyline");
		
		
		BookDetailsController bb = new BookDetailsController();
		
	
		
		bb.setid(id);
		bb.setNameOfBook(NameOfBook);
		bb.setStoryline(Storyline);
		
		
		BookDetailsService bs = new BookDetailsService();
		String res = bs.storeBookDetails(bb);
		pw.println(res);
	}

	private void setid(int id) {
		// TODO Auto-generated method stub
		
	}

	private void setStoryline(String storyline) {
		// TODO Auto-generated method stub
		
	}

	private void setNameOfBook(String nameOfBook) {
		// TODO Auto-generated method stub
		
	}
}
