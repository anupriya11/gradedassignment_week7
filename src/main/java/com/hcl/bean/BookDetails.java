package com.hcl.bean;

public class BookDetails {
	
		private int id;
		private String NameOfBook;
		private String Storyline;
		public BookDetails() {
			
		}
		public BookDetails(int id,String NameOfBook,String Storyline) {
			super();
			this.id=id;
			this.NameOfBook=NameOfBook;
			this.Storyline=Storyline;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getNameOfBook() {
			return NameOfBook;
		}
		public void setNameOfBook(String nameOfBook) {
			NameOfBook = nameOfBook;
		}
		public String getStoryline() {
			return Storyline;
		}
		public void setStoryline(String storyline) {
			Storyline = storyline;
		}

		@Override
		public String toString() {
			return "BookDetails [id=" + id + ", NameOfbook=" + NameOfBook + ", Storyline=" + Storyline + "]";
					
		}
	}


